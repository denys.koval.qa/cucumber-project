package com.api.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DTOResponse {
    public String provider;
    public String title;
    public String url;
    public String brand;
    public double price;
    public String unit;
    public boolean isPromo;
    public String promoDetails;
    public String image;
}

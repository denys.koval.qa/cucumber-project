package com.api.utils;

public enum HttpMethod {
    GET,
    PUT,
    PATCH,
    POST,
    DELETE
}
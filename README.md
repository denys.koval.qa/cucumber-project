# CUCUMBER-STARTER-NEW project

## **Overview:**
After analyzing previous framework I desided to re-write it from scratch

## **Start implement new .feature file:**
1. Go to \src\test\resources\features and create a new .feature file
2. Create a new stepsMethods if needed in SearchStepDefinitions.java
3. In TestRunner.java class add additional CucumberOptions if needed
4. Create docker image using Dockerfile or use existing one [You will use that image in gitlab-ci.yml file]
5. Run tests using command below and to go target/site/allure-maven-plugin and open index.html report
6. Push your changes to GitLab [https://gitlab.com/denys.koval.qa/cucumber-project]
7. Pipeline will start with two stages [run tests and generate report]
8. See report Artifact tab

### **Some of the key features of this framework:**

1. Instead of SerenityRest used RestAssured to make possible work with DTO.
2. It generates Allure report.
3. Test execution can be triggered form command line.
4. Easy integration to CI/CD pipeline.

## **Required Setup :**

- [Java](https://www.guru99.com/install-java.html) should be installed and configured.
- [Maven](https://mkyong.com/maven/how-to-install-maven-in-windows/) should be installed and configured.
- [Docker](https://www.docker.com/) should be installed and configured.

## **Running Test:**

Open the command prompt and navigate to the folder in which pom.xml file is present.
Run the below Maven command.

    mvn clean test allure:report


Once the execution completes report & log will be generated in below folder.

**Report:** 		*target/site/allure-maven-plugin*<br>
**Log:** 		*target/logs*